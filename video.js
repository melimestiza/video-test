$(document).ready(function() {
function myHandler() {
    $("#showdiv").toggle("slow");
}

var runAtTime = function(handler, time) {
     var wrapped = function() {
         if(this.currentTime >= time) {
             $(this).off('timeupdate', wrapped);
             return handler.apply(this, arguments);
        }
     }
    return wrapped;
};

$('#myVideo').on('timeupdate', runAtTime(myHandler, 3)); 


});


